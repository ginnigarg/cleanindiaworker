//
//  LoginViewController.swift
//  CleanIndiaWorker
//
//  Created by Guneet Garg on 27/12/18.
//  Copyright © 2018 Ginni Garg. All rights reserved.
//

import Foundation
import UIKit
import AWSMobileClient

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var pwdTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginButton.clipsToBounds = true
    }
    
    @IBAction func login() {
        let email = emailTextField.text
        let pwd = pwdTextField.text
        if email == "" || pwd == "" {
            self.displayAlert("All fields necessary!!")
            return
        } else {
            
            AWSMobileClient.sharedInstance().signIn(username: email!, password: pwd!) { (signInResult, error) in
                    if let error = error  {
                        DispatchQueue.main.async {
                            self.displayAlert("Please check your credentials!")
                        }
                        print(error)
                        return
                    } else if let signInResult = signInResult {
                        switch (signInResult.signInState) {
                        case .signedIn:
                            print("User is signed in.")
                        case .smsMFA:
                            print("SMS message sent to \(signInResult.codeDetails!.destination!)")
                        default:
                            print("Sign In needs info which is not et supported.")
                        }
                    }
                
                DispatchQueue.main.async {
                    let navigatorController = self.storyboard?.instantiateViewController(withIdentifier: "InitialNavigatorController")
                    self.present(navigatorController!, animated: true, completion: nil)
                }
            }
        }
    }
}
