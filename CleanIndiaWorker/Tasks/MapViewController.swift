//
//  MapViewController.swift
//  CleanIndiaWorker
//
//  Created by Guneet Garg on 27/12/18.
//  Copyright © 2018 Ginni Garg. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    var coords = CLLocationCoordinate2D()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        coords.latitude = 30.301295641270475
        coords.longitude = 75.37156627852588
        addAnnotations()
    }
    
    func addAnnotations() {
        let annotation = MKPointAnnotation()
        annotation.coordinate = coords
        mapView.addAnnotation(annotation)
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        self.performSegue(withIdentifier: "ConfirmTask", sender: self)
    }
    
}
